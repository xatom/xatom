# -*- coding: utf-8 -*-

import unittest
from app.core.config import Config


class TestConfig(unittest.TestCase):
    """
    配置文件测试
    """

    @classmethod
    def setUpClass(cls) -> None:
        cls.config = Config.init()

    def test_get_success(self):
        """测试获取存在的option"""

        value = self.config.get("mysql", "host")
        self.assertIsNotNone(value, "获取值失败")

    def test_get_no_section_error(self):
        """测试获取不存在的section"""

        try:
            value = self.config.get("mysql_no", "host")
        except Exception as e:
            value = "error"

        self.assertEqual(value, "error", "没有获取到值")

    def test_get_no_option_error(self):
        """测试获取不存在的option"""

        try:
            value = self.config.get("mysql", "host1")
        except Exception as e:
            value = "error"

        self.assertEqual(value, "error", "没有获取到值")


if __name__ == "__main__":
    # verbosity=*：默认是1；设为0，则不输出每一个用例的执行结果；2-输出详细的执行结果
    unittest.main(verbosity=2)
