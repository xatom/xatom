# -*- coding: utf-8 -*-

import os
from flask import Flask
from flask_bootstrap import Bootstrap

from app.core.config import Config
from app.helper.const import TEMPLATE_FOLDER, STATIC_FOLDER
from app.helper.helper import root_path
from app.routes.home import home_page
from app.routes.example import example

template_folder = os.path.join(root_path(), TEMPLATE_FOLDER)
static_folder = os.path.join(root_path(), STATIC_FOLDER)

app = Flask(__name__, template_folder=template_folder, static_folder=static_folder)

bootstrap = Bootstrap(app)

#
# 每添加一个路由文件，需要在这里进行注册
#
# 首页
app.register_blueprint(home_page)

# 演示
app.register_blueprint(example, url_prefix='/example')

if __name__ == '__main__':
    config = Config.init()
    host = config.get("web", "host")
    port = config.get("web", "port")
    debug = config.get("web", "debug")

    app.run(host=host, port=port, debug=debug)
