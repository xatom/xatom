# xatom

#### 介绍
基于python3的单元测试框架

#### 软件架构
软件架构说明 


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 开发手册

使用前先操作配置文件

    将config下面的app.ini.example复制一份改名为app.ini
    将config下面的database.ini.example复制一份改名为database.ini
    

1.路由route

    在app/route中新建路由文件，比如：example.py

    然后再public/main.py中注册路由，比如：app.register_blueprint(example, url_prefix='/example')

2.控制器controller

    在app/controller中新建控制器，比如：example_controller.py

3.服务server

    在app/server中新建服务，比如：example_server.py

4.模型model

5.视图view

#### 帮助文档

[前端框架](https://demos.creative-tim.com/light-bootstrap-dashboard/documentation/tutorial-components.html)