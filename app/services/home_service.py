# -*- coding: utf-8 -*-

from app.services.base_service import BaseService


class HomeService(BaseService):
    def __init__(self):
        super().__init__()

    def get_home_message(self):
        return "welcome to xatom!"
