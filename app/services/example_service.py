# -*- coding: utf-8 -*-
from app.services.base_service import BaseService


class ExampleService(BaseService):
    def __init__(self):
        super().__init__()

    def get_data(self):
        return "当你看到这条消息，说明我是后端返回的"
