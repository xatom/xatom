# -*- coding: utf-8 -*-

import os
from os.path import dirname

from app.helper.const import LOGS_FOLDER


def root_path():
    """获取项目根目录"""

    return dirname(dirname(dirname(os.path.realpath(__file__))))


def logs_path():
    """日志路径"""

    path = os.path.join(root_path(), LOGS_FOLDER)
    check_path(path)

    return path


def check_path(path):
    """检查路径，不存在则创建"""

    if not os.path.exists(path):
        os.makedirs(path)
