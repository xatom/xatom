# -*- coding: utf-8 -*-

# 日志存放文件夹
LOGS_FOLDER = "storage/logs"

# 视图模板文件夹
TEMPLATE_FOLDER = "app/views"

# 静态文件文件夹
STATIC_FOLDER = "public/assets"
