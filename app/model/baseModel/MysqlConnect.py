from peewee import *
from app.core.config import Config
from flask import Flask

cf = Config.init()
app = Flask(__name__)
"""
数据库链接配置
"""
mysql_db = MySQLDatabase(cf.get("mysql", "database"),
                         host=cf.get("mysql", "host"),
                         user=cf.get("mysql", "username"),
                         passwd=cf.get("mysql", "password"),
                         port=cf.getint("mysql", "port"),
                         charset="utf8mb4")


@app.before_request
def _db_connect():
    mysql_db.connect()

# This hook ensures that the connection is closed when we've finished
# processing the request.
@app.teardown_request
def _db_close(exc):
    if not mysql_db.is_closed():
        mysql_db.close()