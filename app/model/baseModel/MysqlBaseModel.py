from playhouse.signals import pre_save, Model
from app.model.baseModel.MysqlConnect import mysql_db
from datetime import datetime
from peewee import DateTimeField


class MysqlBaseModel(Model):
    created_at = DateTimeField(default=datetime.now)
    updated_at = DateTimeField()
    class Meta:
        database = mysql_db

@pre_save(sender=MysqlBaseModel)
def pre_save(sender, instance, created):
    if created:
        instance.updated_at = datetime.now()



