# -*- coding: utf-8 -*-
from flask import render_template

from app.controllers.base_controller import BaseController
from app.services.example_service import ExampleService


class ExampleController(BaseController):

    def __init__(self):
        super().__init__()

    def index(self):
        example_server = ExampleService()
        message = example_server.get_data()
        return render_template("example.html", message=message)
