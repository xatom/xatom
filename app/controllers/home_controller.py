# -*- coding: utf-8 -*-

from flask import render_template

from app.controllers.base_controller import BaseController
from app.services.home_service import HomeService


class HomeController(BaseController):
    """首页控制器"""

    def __init__(self):
        super().__init__()

    def index(self):
        """首页路由"""

        home_server = HomeService()
        name = home_server.get_home_message()

        return render_template("home.html",name=name)
