# -*- coding: utf-8 -*-

from flask import Blueprint

from app.controllers.home_controller import HomeController

home_page = Blueprint('home_page', __name__)


@home_page.route('/', methods=["GET"])
def home():
    return HomeController().index()
