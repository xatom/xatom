# -*- coding: utf-8 -*-

from flask import Blueprint

from app.controllers.example_controller import ExampleController

example = Blueprint('example', __name__)


@example.route('/', methods=["GET"])
def index():
    return ExampleController().index()
